﻿//RDM_Ball behaviour

using UnityEngine;

public class CF_Ball : MonoBehaviour {
    //static ref
    public static CF_Ball controller;

    //Targets
    [Range(0,10f)]
    [SerializeField] float bSpeed = 1;
    [Range(0,1f)]
    [SerializeField] float bRampUp = 0.001f;
    
    //Moving Vars
    float speedRamp;
    
    //Init Vars
    Vector3 startPos;
    Quaternion startRot;
    

    [Header("Audio")]
    [SerializeField] AudioClip hitPaddle;
    [SerializeField] AudioClip hitWall;
    [SerializeField] AudioClip hitAbyss;
    AudioSource voice;

    //Rigidbody
    Rigidbody rb;

    //Initialize
    void Awake () {
        voice = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();
        controller = this;
        startPos = transform.position;
        startRot = transform.rotation;
        speedRamp = 0;
        resetBall();
    }

    //Abyss Ball, reset
    public void resetBall(){
        if(CF_Controller.controller && CF_Controller.controller.resetting){return; } //wait if resetting from GameOver
        speedRamp = bSpeed;
        transform.position = startPos;
    }

    void FixedUpdate() {
        ballMotion();
        constrainAng();
    }

    void ballMotion(){
        transform.Translate(0,0,speedRamp,Space.Self); //ForwardMotion
        speedRamp += bRampUp;//Speed ramping up for challenge
    }

    //rigidbody develops angular velocity, murder it with maths
    void constrainAng(){
        rb.angularVelocity = Vector3.zero;
    }

    //debug plus incident angle
    Vector3 reflectedAngle(Vector3 angleIn, Vector3 normIn){
        Debug.DrawRay(transform.position,normIn,Color.red,2);
        Debug.DrawRay(transform.position,angleIn,Color.blue,2);
        Debug.DrawRay(transform.position,Vector3.Reflect(angleIn,normIn), Color.yellow,3);
        return Vector3.Reflect(angleIn,normIn);
    }

    void setHitLook(Collision collision) {
        Quaternion lookRot = new Quaternion(); //SetLookRotation only works with new declared var.
        lookRot.SetLookRotation(reflectedAngle(transform.forward,collision.contacts[0].normal),Vector3.up);//Force in direction of incident angle.
        transform.rotation = lookRot;
    }

    //Audio
    public void play_Wall() {
        voice.clip = hitWall;
        voice.Play();
    }
    public void play_Abyss() {
        voice.clip = hitAbyss;
        voice.Play();
    }
    public void play_Paddle() {
        voice.clip = hitPaddle;
        voice.Play();
    }

    void OnCollisionEnter(Collision collision) {
        Debug.Log("Ball Contact");
        //Bounce angle
        setHitLook(collision);

        //Abyss
        if(collision.collider.CompareTag("Respawn")){
            resetBall();
            transform.rotation = Random.rotation;
            CF_Controller.controller.loseLife();
            play_Abyss();
        }
        else if(collision.collider.CompareTag("Player")) { // Paddle
            play_Paddle();
        }
        else{ //Everything else
            play_Wall();
        }

    }

   

}
