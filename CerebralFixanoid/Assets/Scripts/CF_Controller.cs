﻿//RDM_Controller handles gameplay, score, lives, etc. Should initiate and end game.

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CF_Controller : MonoBehaviour {
    //static ref
    public static CF_Controller controller; 
    
    [Header("Scoreboard")]
    [SerializeField] Text scoreText;
    [SerializeField] Text livesText;
    [SerializeField] Text Title;

    //adjustable
    [Range(100,10000)]
    public int lifeIncrement = 1000;//amount until new life

    //timed
    public int score = 0;
    int lifeIncrementor;
    
    //lives
    int lives;

    //gameover flag
    public bool resetting;

    //initialize
	void OnEnable () {
        controller = this;
        score = 0;
        lives = 3;
        lifeIncrementor = 0;
    }

	void FixedUpdate () {
        if(resetting){return; } //return out of score if resetting
        addscore();
        scoreText.text = "Score: " + score.ToString();
        livesText.text = "Lives: " + lives.ToString();

        //Game Over
        if(lives <= 0){
            StartCoroutine(resetGame());
        }
    }


    //Reset Process
    IEnumerator resetGame(){
        resetting = true;
        Title.text = "GameOver";
        CF_Ball.controller.gameObject.SetActive(false);
        yield return new WaitForSeconds(5);
        score = 0;
        lives = 3;
        lifeIncrementor = 0;
        resetting = false;
        CF_Ball.controller.resetBall();
        Title.text = "TESSARACTANOID";
        CF_Ball.controller.gameObject.SetActive(true);
    }

    //score increased as game proceeds, give an extra life if score goes past a certain point.
    public void addscore(){
        score++;
        lifeIncrementor++;
        if(lifeIncrementor > lifeIncrement) {
            lives++;
            lifeIncrementor = 0;
        }
    }

    public void loseLife(){
        lives--;
    }
}
