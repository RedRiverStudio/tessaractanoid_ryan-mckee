﻿//RDM_Paddle behaviour.  Paddle is moved by mouse, Range can be changed for differen levels, but temp set for level 1

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CF_Paddle : MonoBehaviour {

    [Header("PaddleRange")]
    [Range(0.1f,2f)]
    [SerializeField] float paddleSens = 1; // Sensitivity
    [Range(5,10)]
    [SerializeField] float paddleRange = 1; //Range Block of Paddle

    float paddleX = 0;
    float paddleY = 0;
	
	void Update () {
        paddleInput();
        paddleMovement();
    }

    //Inputs
    void paddleInput(){
        //Mouse Only
        paddleX = Input.GetAxis("Mouse X") * -paddleSens;
        paddleY = Input.GetAxis("Mouse Y") * -paddleSens;
        //Can add more inputs BUT OUT OF TIME
    }

    //Apply motions and clamp
    void paddleMovement(){
        transform.localPosition += new Vector3(paddleX,0,paddleY);
        transform.localPosition = new Vector3(Mathf.Clamp(transform.position.x,-paddleRange,paddleRange),0,Mathf.Clamp(transform.position.z,-paddleRange,paddleRange));
    }
}
