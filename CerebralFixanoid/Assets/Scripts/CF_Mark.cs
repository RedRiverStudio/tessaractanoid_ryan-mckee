﻿using UnityEngine;

public class CF_Mark : MonoBehaviour {
    CF_Ball ball;
    public wallSides wallside;
    public enum wallSides{Left,Right,Front,Back,Top,Bottom}
    const int edgeLength = 10;

    void Start() {
        ball = CF_Ball.controller;
    }

    void Update () {
        if(ball){transform.position = ball.transform.position; }
        wallConstraint();
    }

    void wallConstraint(){
        Vector3 conPos = transform.position;
        if(wallside == wallSides.Left) {
            conPos.x = edgeLength;
        }
        else if(wallside == wallSides.Right) {
            conPos.x = -edgeLength;
        }
        else if(wallside == wallSides.Front) {
            conPos.z = -edgeLength;
        }
        else if(wallside == wallSides.Back) {
            conPos.z = edgeLength;
        }
        else if(wallside == wallSides.Top) {
            conPos.y = edgeLength * 2;
        }
        else if(wallside == wallSides.Bottom) {
            conPos.y = 0;
        }
        transform.position = conPos;
    }
}
